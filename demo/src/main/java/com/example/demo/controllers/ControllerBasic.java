package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.configuration.Pages;
import com.example.demo.model.Post;

@Controller
@RequestMapping("/home")
public class ControllerBasic {
	
	public List<Post> getPosts(){
		ArrayList<Post> post = new ArrayList<>(); //crear una lista de arreglos de post
		
		//crear el primer arreglo de post, utilizando en cosntructor Post() del modelo Post.java
		post.add(new Post(1, "Desarrollo web es un término que define la creación de sitios web para Internet o una intranet.", 
				"http://localhost:8080/img/Post01.jpg", new Date(), "Desarrollo web con Spring"));
		
		post.add(new Post(2, "Desarrollo web es un término que define la creación de sitios web para Internet o una intranet.", 
				"http://localhost:8080/img/Post01.jpg", new Date(), "Desarrollo web front-end"));
		
		post.add(new Post(3, "Desarrollo web es un término que define la creación de sitios web para Internet o una intranet.", 
				"http://localhost:8080/img/Post01.jpg", new Date(), "Desarrollo web Back-end"));
		
		post.add(new Post(4, "Desarrollo web es un término que define la creación de sitios web para Internet o una intranet.", 
				"http://localhost:8080/img/Post01.jpg", new Date(), "Desarrollo web con UI - UX"));
		
		return post; //retornamos el arreglo de la lista
		
	}
	
	
	@GetMapping(path = {"/post","/"}) 
	public String saludar(Model model) { // Model es una interfaz para anadir atributos del modelo
		model.addAttribute("posts", this.getPosts()); //anadimos atributos del modelo con el metodo model.addAtribute()
		return "index"; 
	}
	
	
	//agregando el modelo and view (se agrega el modelo y la vista a la funcion)
	@GetMapping(path = "/test")
	public ModelAndView post() {
		ModelAndView modelAndView = new ModelAndView(Pages.HOME);
		modelAndView.addObject("posts", this.getPosts());
		return modelAndView;
	}
	
	

}