package com.example.demo.model;

import java.util.Date;

public class Post {
	//atributos de mi modelo
	private int id;
	private String descripcion;
	private String urlImg;
	private Date fecha;
	private String titulo;
	
	//constructor de la clase post clic derecho y seleccionar source -> generate constructor
	public Post(int id, String descripcion, String urlImg, Date fecha, String titulo) {
	
		this.id = id;
		this.descripcion = descripcion;
		this.urlImg = urlImg;
		this.fecha = fecha;
		this.titulo = titulo;
	}
	
	//constructor vacio
	public Post() {
		
	}

    //los metodos geter y seter clic derecho y seleccionar source -> generate geter y seter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUrlImg() {
		return urlImg;
	}
	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	

}
